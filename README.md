This projects runs timescaledb into a docker container and contains SQL table to upload Tor network descriptors
and process their data.

For further instructions on timescaledb please visit:
https://docs.timescale.com/timescaledb/latest/getting-started/launch-timescaledb/

For furter instruction on timescaledb on docker please visit:
https://docs.timescale.com/timescaledb/latest/how-to-guides/install-timescaledb/self-hosted/docker/installation-docker/

The run script will start the docker container with timescale exposed only on localhost. Please exec:
```
$ ./bin/run
```
