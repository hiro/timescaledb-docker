/*
 * The digest in the bridge_network_status table needs to be created
 * programmatically because it is not part of the bridge network status
 * document.
 */

CREATE TABLE IF NOT EXISTS bridge_network_status(
  published               TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint             TEXT                         NOT NULL,
  flag_thresholds         TEXT,
  stable_uptime           BIGINT,
  stable_mtbf             BIGINT,
  fast_bandwidth          BIGINT,
  guard_wfu               DOUBLE PRECISION,
  guard_tk                BIGINT,
  guard_bandwidth_including_exits BIGINT,
  guard_bandwidth_excluding_exits BIGINT,
  enough_mtbf_info        INTEGER,
  ignore_adv_bws          INTEGER,
  header                  TEXT,
  digest                  TEXT                         NOT NULL,
  PRIMARY KEY(digest));

CREATE TABLE IF NOT EXISTS bridge_status(
  published               TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint             TEXT                         NOT NULL,
  nickname                TEXT                         NOT NULL,
  digest                  TEXT                         NOT NULL,
  network_status          TEXT references bridge_network_status(digest),
  address                 TEXT,
  or_port                 INTEGER,
  dir_port                INTEGER,
  or_address              TEXT,
  flags                   TEXT,
  bandwidth               BIGINT,
  policy                  TEXT,
  PRIMARY KEY(digest));
