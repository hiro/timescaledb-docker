/*
 * Digests in bridge_pool_assignments has to be created programmatically.
 */

CREATE TABLE IF NOT EXISTS bridge_pool_assignments_file(
   published                 TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   header                    TEXT                          NOT NULL,
   digest                    TEXT                          NOT NULL,
   PRIMARY KEY(digest)
);


CREATE TABLE IF NOT EXISTS bridge_pool_assignment(
   published                 TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   distribution_method       TEXT                          NOT NULL,
   transports                TEXT,
   ip                        TEXT,
   blocklist                 TEXT,
   bridge_pool_assignments   TEXT references bridge_pool_assignments_file(digest)
);
