/*
 * Digests in bridgestrap_stats has to be created programmatically.
 */

CREATE TABLE IF NOT EXISTS bridgestrap_stats(
  bridgestrap_stats_end         TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  interval                      BIGINT                       NOT NULL,
  digest                        TEXT,
  cached_requests               BIGINT,
  header                        TEXT,
  PRIMARY KEY(digest)
);

CREATE TABLE IF NOT EXISTS bridgestrap_test(
  published                     TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  result                        BOOLEAN                      NOT NULL,
  fingerprint                   TEXT                         NOT NULL,
  bridgestrap_stats             TEXT references bridgestrap_stats(digest)
);
