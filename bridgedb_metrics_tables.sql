/*
 * Digests in bridgedb_metrics have to be created programmatically.
 */

CREATE TABLE IF NOT EXISTS bridgedb_metrics(
  bridgedb_metrics_end          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  interval                      BIGINT                       NOT NULL,
  digest                        TEXT,
  version                       TEXT,
  PRIMARY KEY(digest)
);

CREATE TABLE IF NOT EXISTS bridgedb_metrics_count(
  time                          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  distribution                  TEXT                         NOT NULL,
  transport                     TEXT                         NOT NULL,
  country                       TEXT                         NOT NULL,
  status                        TEXT                         NOT NULL,
  tests                          TEXT                         NOT NULL,
  value                         BIGINT                       NOT NULL,
  metrics                       TEXT references bridgedb_metrics(digest)
);

SELECT create_hypertable('bridgedb_metrics_count','time');
