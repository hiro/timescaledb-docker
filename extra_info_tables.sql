CREATE TABLE IF NOT EXISTS extra_info_descriptor(
is_bridge                           BOOLEAN                      NOT NULL,
published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
nickname                            TEXT                         NOT NULL,
fingerprint                         TEXT                         NOT NULL,
digest_sha1_hex                     TEXT                         NOT NULL,
identity_ed25519                    TEXT                         NOT NULL,
master_key_ed25519                  TEXT                         NOT NULL,
read_history                        TEXT,
write_history                       TEXT,
ipv6_read_history                   TEXT,
ipv6_write_history                  TEXT,
geoip_db_digest                     TEXT,
geoip6_db_digest                    TEXT,
geoip_start_time                    TIMESTAMP WITHOUT TIME ZONE,
geoip_client_origins                TEXT,
bridge_stats_end                    TIMESTAMP WITHOUT TIME ZONE,
bridge_stats_inverval               BIGINT,
bridge_ips                          TEXT,
bridge_ip_versions                  TEXT,
bridge_ip_transports                TEXT,
dirreq_stats_end                    TIMESTAMP WITHOUT TIME ZONE,
dirreq_stats_interval               BIGINT,
dirreq_v2_ips                       TEXT,
dirreq_v3_ips                       TEXT,
dirreq_v2_reqs                      TEXT,
dirreq_v3_reqs                      TEXT,
dirreq_v2_share                     DOUBLE PRECISION,
dirreq_v3_share                     DOUBLE PRECISION,
dirreq_v2_resp                      TEXT,
dirreq_v3_resp                      TEXT,
dirreq_v2_direct_dl                 TEXT,
dirreq_v3_direct_dl                 TEXT,
dirreq_v2_tunneled_dl               TEXT,
dirreq_v3_tunneled_dl               TEXT,
dirreq_read_history                 TEXT,
dirreq_write_history                TEXT,
entry_stats_end                     TIMESTAMP WITHOUT TIME ZONE,
entry_stats_interval                BIGINT,
entry_ips                           TEXT,
cell_stats_end                      TIMESTAMP WITHOUT TIME ZONE,
cell_stats_interval                 BIGINT,
cell_processed_cells                INTEGER[],
cell_queued_cells                   DOUBLE PRECISION[],
cell_time_in_queue                  DOUBLE PRECISION[],
cell_circuits_per_decile            BIGINT,
conn_bi_direct_timestamp            TIMESTAMP WITHOUT TIME ZONE,
conn_bi_direct_interval             BIGINT,
conn_bi_direct_below                BIGINT,
conn_bi_direct_read                 BIGINT,
conn_bi_direct_write                BIGINT,
conn_bi_direct_both                 BIGINT,
ipv6_conn_bi_direct_timestamp       TIMESTAMP WITHOUT TIME ZONE,
ipv6_conn_bi_direct_interval        BIGINT,
ipv6_conn_bi_direct_below           BIGINT,
ipv6_conn_bi_direct_read            BIGINT,
ipv6_conn_bi_direct_write           BIGINT,
ipv6_conn_bi_direct_both            BIGINT,
exit_stats_end                      TIMESTAMP WITHOUT TIME ZONE,
exit_stats_inverval                 BIGINT,
exit_kibibytes_written              TEXT,
exit_kibibytes_read                 TEXT,
exit_streams_opened                 TEXT,
hidserv_stats_end                   TIMESTAMP WITHOUT TIME ZONE,
hidserv_stats_interval              BIGINT,
hidserv_v3_stats_end                TIMESTAMP WITHOUT TIME ZONE,
hidserv_v3_stats_interval           BIGINT,
hidserv_rend_relayed_cells_value    DOUBLE PRECISION,
hidserv_rend_relayed_cells          TEXT,
hidserv_rend_v3_relayed_cells_value DOUBLE PRECISION,
hidserv_rend_v3_relayed_cells       TEXT,
hidserv_dir_onions_seen_value       DOUBLE PRECISION,
hidserv_dir_onions_seen             TEXT,
hidserv_dir_v3_onions_seen_value    DOUBLE PRECISION,
hidserv_dir_v3_onions_seen          TEXT,
transports                          TEXT,
padding_counts_timestamp            TIMESTAMP WITHOUT TIME ZONE,
padding_counts_interval             BIGINT,
padding_counts                      TEXT,
overload_ratelimits_version         INTEGER,
overload_ratelimits_timestamp       TIMESTAMP WITHOUT TIME ZONE,
overload_ratelimits_ratelimit       BIGINT,
overload_ratelimits_burstlimit      BIGINT,
overload_ratelimits_readcount       BIGINT,
overload_ratelimits_write_count     BIGINT,
overload_fd_exhausted_version       INTEGER,
overload_fd_exhausted_timestamp     TIMESTAMP WITHOUT TIME ZONE,
router_sig_ed25519                  TEXT                         NOT NULL,
router_signature                    TEXT                         NOT NULL,
header                              TEXT                         NOT NULL,
PRIMARY KEY(digest_sha1_hex)
);

CREATE TABLE IF NOT EXISTS read_bandwidth_metric (
   time                      TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   nickname                  TEXT                          NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   read_bandwidth            BIGINT,
   extra_info_descriptor_digest     TEXT references extra_info_descriptor(digest_sha1_hex)
);

SELECT create_hypertable('read_bandwidth_metric','time');

CREATE TABLE IF NOT EXISTS write_bandwidth_metric (
   time                      TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   nickname                  TEXT                          NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   write_bandwidth           BIGINT,
   extra_info_descriptor_digest     TEXT references extra_info_descriptor(digest_sha1_hex)
);

SELECT create_hypertable('write_bandwidth_metric','time');

CREATE TABLE IF NOT EXISTS ipv6_read_bandwidth_metric (
   time                      TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   nickname                  TEXT                          NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   ipv6_read_bandwidth       BIGINT,
   extra_info_descriptor_digest     TEXT references extra_info_descriptor(digest_sha1_hex)
);

SELECT create_hypertable('ipv6_read_bandwidth_metric','time');

CREATE TABLE IF NOT EXISTS ipv6_write_bandwidth_metric (
   time                      TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   nickname                  TEXT                          NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   ipv6_write_bandwidth      BIGINT,
   extra_info_descriptor_digest     TEXT references extra_info_descriptor(digest_sha1_hex)
);

SELECT create_hypertable('ipv6_write_bandwidth_metric','time');

CREATE TABLE IF NOT EXISTS dirreq_read_bandwidth_metric (
   time                      TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   nickname                  TEXT                          NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   dirreq_read_bandwidth     BIGINT,
   extra_info_descriptor_digest     TEXT references extra_info_descriptor(digest_sha1_hex)
);

SELECT create_hypertable('dirreq_read_bandwidth_metric','time');

CREATE TABLE IF NOT EXISTS dirreq_write_bandwidth_metric (
   time                      TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
   nickname                  TEXT                          NOT NULL,
   fingerprint               TEXT                          NOT NULL,
   dirreq_write_bandwidth    BIGINT,
   extra_info_descriptor_digest     TEXT references extra_info_descriptor(digest_sha1_hex)
);

SELECT create_hypertable('dirreq_write_bandwidth_metric','time');
